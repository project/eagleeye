core = 8.x
api = 2
defaults[projects][subdir] = contrib

projects[admin_toolbar][type] = module
projects[admin_toolbar][version] = 1.27
projects[adminimal_admin_toolbar][type] = module
projects[adminimal_admin_toolbar][version] = 1.10
projects[context][type] = module
projects[context][version] = 4.0-beta2
projects[ctools][type] = module
projects[ctools][version] = 3.2
projects[entity][type] = module
projects[entity][version] = 1.0-rc3
projects[token][type] = module
projects[token][version] = 1.5

