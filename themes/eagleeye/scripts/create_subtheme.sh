#!/bin/bash
# Script to quickly create sub-theme.

echo '
+------------------------------------------------------------------------+
| With this script you could quickly create eagleeye sub-theme     |
| In order to use this:                                                  |
| - eagleeye theme (this folder) should be in the contrib folder   |
+------------------------------------------------------------------------+
'
echo 'The machine name of your custom theme? [e.g. mycustom_eagleeye]'
read CUSTOM_eagleeye

echo 'Your theme name ? [e.g. My custom eagleeye]'
read CUSTOM_eagleeye_NAME

if [[ ! -e ../../custom ]]; then
    mkdir ../../custom
fi
cd ../../custom
cp -r ../contrib/eagleeye $CUSTOM_eagleeye
cd $CUSTOM_eagleeye
for file in *eagleeye.*; do mv $file ${file//eagleeye/$CUSTOM_eagleeye}; done
for file in config/*/*eagleeye.*; do mv $file ${file//eagleeye/$CUSTOM_eagleeye}; done
mv {_,}$CUSTOM_eagleeye.theme
grep -Rl eagleeye .|xargs sed -i '' -e "s/eagleeye/$CUSTOM_eagleeye/"
sed -i '' -e "s/SASS Bootstrap Starter Kit Subtheme/$CUSTOM_eagleeye_NAME/" $CUSTOM_eagleeye.info.yml
echo "# Check the themes/custom folder for your new sub-theme."
